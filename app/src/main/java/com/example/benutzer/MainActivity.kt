package com.example.benutzer

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation

class MainActivity : AppCompatActivity() {
    lateinit var out: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        out = findViewById(R.id.logout)

        out.setOnClickListener{
            Navigation.findNavController(this,
                R.id.fragment_container_view
            ).navigate(R.id.loginFragment)
        }
    }
}