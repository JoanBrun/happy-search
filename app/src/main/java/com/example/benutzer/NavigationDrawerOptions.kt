package com.example.benutzer

import android.app.Activity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import com.google.android.material.navigation.NavigationView
import com.example.benutzer.R

object NavigationDrawerOptions {

    fun navigationOptions(navigationView : NavigationView, drawerLayout: DrawerLayout, activity: Activity){
        navigationView.setNavigationItemSelectedListener { menuItem ->
            when(menuItem.itemId){
                R.id.marcas ->Navigation.findNavController(activity, R.id.fragment_container_view).navigate(
                    R.id.marcasFragment
                )            }
            drawerLayout.close()
            true
        }
    }
}