package com.example.benutzer.data.model


data class User(
    val id: Int,
    val name: String,
    val vorname: String,
    val pass: String,
    val email: String,
    val phone: String,
    val dni: String,
    val erlauben: Boolean
)
