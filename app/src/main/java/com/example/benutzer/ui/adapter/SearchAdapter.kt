package com.example.benutzer.ui.adapter

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.benutzer.R
import com.example.benutzer.data.model.User
import com.example.benutzer.ui.view.MarcasFragmentDirections

class SearchAdapter(private var marcas: MutableList<User>) :  RecyclerView.Adapter<SearchAdapter.ListaViewHolder>(){

    private var list = mutableListOf<User>()
    private var listFiltered = mutableListOf<User>()

    inner class ListaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var nom: TextView
        private lateinit var tanca : ImageView
        private var img : ImageView

        init {
            nom = itemView.findViewById(R.id.nom)
            img = itemView.findViewById(R.id.img)
        }

        fun bindData(marca: User, index: Int) {
//            var db = Firebase.firestore
//            val user = FirebaseAuth.getInstance().currentUser
//
//            var bitmap : Bitmap? = null
//            val storage = FirebaseStorage.getInstance().reference.child(user?.email + "/marcas/" + marca.id)
//            val localFile = File.createTempFile("temp", "jpeg")
//            storage.getFile(localFile).addOnSuccessListener {
//                bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
//            }

//            nom.text = marca.nom
//            Handler().postDelayed({
//                img.setImageBitmap(bitmap)
//                marca.foto = bitmap}, 1000)

            tanca = itemView.findViewById(R.id.tanca)
            tanca.setOnClickListener {deleteMarca(index)}
        }
    }
    @SuppressLint("NotifyDataSetChanged")
    fun deleteMarca(index: Int){
        var marker = marcas[index]

        marcas.removeAt(index)
        notifyDataSetChanged()

//        val user = FirebaseAuth.getInstance().currentUser
//        val db = Firebase.firestore
//
//        db.collection("users").document(user?.email.toString()).collection("marcas").document(marker.id.toString()).delete()
//        val storage = Firebase.storage
//        val storageRef = storage.reference
//        storageRef.child(user?.email + "/marcas/${marker.id}").delete()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user, parent, false)
        return ListaViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListaViewHolder, position: Int) {
        holder.bindData(marcas[position], position)

        holder.itemView.setOnClickListener {
            val directions = MarcasFragmentDirections.actionMarcasFragmentToMarcasDetails(marcas[position].id)
            Navigation.findNavController(it).navigate(directions)
        }

    }
    
    fun filtrar(s: String){
        if (s.length == 0){
            listFiltered.clear()
            listFiltered.addAll(list)
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                var lista = listFiltered.stream()
//                    .filter { i -> i.nom.toLowerCase().contains(s.toLowerCase()) }
//                    .collect(Collectors.toList());
//                listFiltered.clear()
//                listFiltered.addAll(lista)
            }else {
                for (mark in list) {
//                    if (mark.nom.toLowerCase().contains(s.toLowerCase())) {
//                        listFiltered.add(mark)
//                    }
                }
            }
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return marcas.size
    }
}