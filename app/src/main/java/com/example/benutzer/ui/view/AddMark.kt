package com.example.benutzer.ui.view

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.core.view.drawToBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.benutzer.R
import com.example.benutzer.data.model.User
import com.example.benutzer.ui.viewModel.SearchViewModel
import java.io.ByteArrayOutputStream


class AddMark : Fragment(R.layout.add_user_fragment) {
    private val viewModel: SearchViewModel by activityViewModels()

    lateinit var coord : EditText
    lateinit var nom : EditText
    lateinit var loc : EditText
    lateinit var btnCamera: Button
    lateinit var img: ImageView
    lateinit var addTareaButton: ImageView

    private var marca: User? = null
    private var marcaPosition: Int? = null

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        coord = view.findViewById(R.id.cordenada)
        nom = view.findViewById(R.id.nom)
        loc = view.findViewById(R.id.loc)
        btnCamera = view.findViewById(R.id.btncamera)
        img = view.findViewById(R.id.foto)

        addTareaButton = view.findViewById(R.id.add_mark)

        coord.setText("${arguments?.getFloat("latitud")} , ${arguments?.getFloat("longitud")}")

        btnCamera.setOnClickListener{
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                startActivityForResult(takePictureIntent, 1)
            } catch (e: ActivityNotFoundException) {
            }
        }

        addTareaButton.setOnClickListener {
            val nom = nom.text.toString()
            val loc = loc.text.toString()
            val foto = img.drawToBitmap()

            var posMarca = 1
            if (viewModel.getUsers().isEmpty()){
                posMarca = 1

            }else{
                posMarca = viewModel.getUsers().last().id + 1
            }


//            marca = Mark(
//                posMarca,
//                LatLng(arguments?.getFloat("latitud")!!.toDouble() , arguments?.getFloat("longitud")!!.toDouble()),
//                nom,
//                loc,
//                foto
//            )
//            viewModel.users.add(user!!)
            //viewModel.add(marca!!)


//            var db = Firebase.firestore
//            val user = FirebaseAuth.getInstance().currentUser
//            val storage = Firebase.storage
//            val storageRef = storage.reference
//            val marcasRef = storageRef.child(user?.email + "/marcas/" + posMarca)

//            db.collection("users").document(user?.email.toString()).collection("marcas").document(
//                posMarca.toString()).set(
//                    hashMapOf(
//                        "id" to posMarca,
//                        "coord" to GeoPoint(marca!!.cordenadas.latitude, marca!!.cordenadas.longitude),
//                        "nom" to nom,
//                        "loc" to loc
//                    )
//                )

            val baos = ByteArrayOutputStream()
            foto.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()

//            marcasRef.putBytes(data)


//            findNavController().navigate(R.id.action_addMark_to_mapFragment)

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            val imageBitmap = data!!.extras!!.get("data") as Bitmap
            img.setImageBitmap(imageBitmap)
        }
    }



}