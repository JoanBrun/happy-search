package com.example.benutzer.ui.view
import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.benutzer.NavigationDrawerOptions
import com.example.benutzer.R
import com.example.benutzer.ui.viewModel.SearchViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.navigation.NavigationView

class MapFragment : Fragment(), OnMapReadyCallback {
    lateinit var map: GoogleMap
    lateinit var afegir: ImageView

    private val viewModel: SearchViewModel by activityViewModels()

    private lateinit var vieww: NavigationView
    private lateinit var lay: DrawerLayout
    private lateinit var obrir: ImageView

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setMapStyle(context?.let { MapStyleOptions.loadRawResourceStyle(it, R.raw.mapablack) })
        createMarker()
        enableLocation()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView =  inflater.inflate(R.layout.map_fragment, container, false)
        createMap()
        return rootView
    }


    fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as     SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    fun createMarker(){
        for (markers in viewModel.getUsers()){
//            val coordinates = markers.cordenadas
//            val myMarker = MarkerOptions().position(coordinates).title(markers.nom)
//            map.addMarker(myMarker)
        }
//        map.animateCamera(
//            CameraUpdateFactory.newLatLngZoom(coordinates, 18f),
//            5000, null)
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    val REQUEST_CODE_LOCATION = 100
    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }
    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        afegir = view.findViewById(R.id.add_mark)
        vieww = activity?.findViewById(R.id.barra)!!
        lay = requireActivity().findViewById(R.id.drawerLayout)
        obrir = view.findViewById(R.id.obrirMenu)

        NavigationDrawerOptions.navigationOptions(vieww, lay, requireActivity())

        afegir.setOnClickListener{
            val latitude = map.myLocation.latitude
            val longitude = map.myLocation.longitude
//            findNavController().navigate(R.id.action_mapFragment_to_addMark, bundleOf("latitud" to latitude.toFloat(), "longitud" to longitude.toFloat()))
        }
        obrir.setOnClickListener{
            lay.openDrawer(GravityCompat.START)
        }

        Handler().postDelayed({
            map.setOnMapLongClickListener {
//                findNavController().navigate(R.id.action_mapFragment_to_addMark, bundleOf("latitud" to it.latitude.toFloat(), "longitud" to it.longitude.toFloat()))
            }
        }, 800)
    }
}