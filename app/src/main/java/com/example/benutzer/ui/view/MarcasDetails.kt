package com.example.benutzer.ui.view

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.core.view.drawToBitmap
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.benutzer.R
import com.example.benutzer.ui.viewModel.SearchViewModel
import com.google.android.material.navigation.NavigationView
import androidx.navigation.fragment.findNavController
import com.example.benutzer.data.model.User
//import com.google.firebase.auth.FirebaseAuth
//import com.google.firebase.firestore.ktx.firestore
//import com.google.firebase.ktx.Firebase
//import com.google.firebase.storage.ktx.storage
import java.io.ByteArrayOutputStream


class MarcasDetails : Fragment(R.layout.users_details) {
    private val viewModel: SearchViewModel by activityViewModels()

    lateinit var coord : EditText
    lateinit var nom : EditText
    lateinit var loc : EditText
    lateinit var btnCamera: Button
    lateinit var img: ImageView
    lateinit var updateMarca: ImageView
    lateinit var detras: ImageView

    private var marcaa: User? = null
    private var marcaPosition: Int? = null

    private lateinit var menu: NavigationView
    private lateinit var lay: DrawerLayout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        coord = view.findViewById(R.id.cordenada)
        nom = view.findViewById(R.id.nom)
        loc = view.findViewById(R.id.loc)
        btnCamera = view.findViewById(R.id.btncamera)
        img = view.findViewById(R.id.foto)

        updateMarca = view.findViewById(R.id.update)
        detras = view.findViewById(R.id.atras)

        detras.setOnClickListener {
            findNavController().navigate(R.id.action_marcasDetails_to_marcasFragment)
        }

        val idMarca = arguments?.getInt("idMarca")
        val marca = idMarca?.let { viewModel.getUser(it) }
//        val lat = marca?.cordenadas?.latitude
//        val long = marca?.cordenadas?.longitude
//        val foto = marca?.foto

//        coord.text = SpannableStringBuilder(lat.toString() + long.toString())
//        nom.text = SpannableStringBuilder(marca?.nom)
//        loc.text = SpannableStringBuilder(marca?.loc)
//        img.setImageBitmap(foto)

        btnCamera.setOnClickListener{
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                startActivityForResult(takePictureIntent, 1)
            } catch (e: ActivityNotFoundException) {
            }
        }

        updateMarca.setOnClickListener {
            val nom = nom.text.toString()
            val loc = loc.text.toString()
            val foto = img.drawToBitmap()


//            marca!!.nom = nom
//            marca.loc = loc
//            marca.foto = foto

//            var db = Firebase.firestore
//            val user = FirebaseAuth.getInstance().currentUser
//
//            val storage = Firebase.storage
//            val storageRef = storage.reference
//            val marcasRef = storageRef.child(user?.email + "/marcas/" + marca?.id)
            val baos = ByteArrayOutputStream()
            foto.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()

//            marcasRef.putBytes(data)
//
//            db.collection("users").document(user?.email.toString()).collection("marcas").document(
////                marca?.id.toString()).update(
////                mapOf(
////                    "nom" to nom,
////                    "loc" to loc
////                )
//            )


            findNavController().navigate(R.id.action_marcasDetails_to_marcasFragment)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            val imageBitmap = data!!.extras!!.get("data") as Bitmap
            img.setImageBitmap(imageBitmap)
        }
    }
}