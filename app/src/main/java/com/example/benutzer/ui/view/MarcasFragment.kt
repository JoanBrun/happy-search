package com.example.benutzer.ui.view

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.benutzer.NavigationDrawerOptions
import com.example.benutzer.R
import com.example.benutzer.ui.adapter.SearchAdapter
import com.google.android.material.navigation.NavigationView


class MarcasFragment: Fragment(R.layout.users_view) {
    private lateinit var recyclerView: RecyclerView
//    private val viewModel: SearchViewModel by activityViewModels()

    private lateinit var menu: NavigationView
    private lateinit var lay: DrawerLayout
    private lateinit var obrir: ImageView
    private lateinit var search : EditText

    private lateinit var adap: SearchAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.recycler_view)

        menu = activity?.findViewById(R.id.barra)!!
        lay = requireActivity().findViewById(R.id.drawerLayout)
        obrir = view.findViewById(R.id.obrirMenu)
        search = view.findViewById(R.id.buscador)

//        search.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable) {}
//            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    adap.filtrar(s.toString())
//                }
//            }
//        })



        val linear = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        recyclerView.layoutManager = linear

//        recyclerView.adapter = SearchAdapter(viewModel.getUsers())


        NavigationDrawerOptions.navigationOptions(menu, lay, requireActivity())

        obrir.setOnClickListener{
            lay.openDrawer(GravityCompat.START)
        }
    }

}
