package com.example.benutzer.ui.viewModel

import androidx.lifecycle.ViewModel
import com.example.benutzer.data.model.User
import java.sql.DriverManager


class SearchViewModel() : ViewModel()  {
    var users = mutableListOf<User>()

    var user: User? = null
    
//    fun deleteMarca(marca: Mark) = users.remove(marca)

    init {
        val url = "jdbc:mysql://127.0.0.1/login"
        val username = "root"
        val password = "yalp"

        val connection = DriverManager.getConnection(url, username, password)
        val statement = connection.createStatement()
        val query = "SELECT * FROM users"
        val resultSet = statement.executeQuery(query)

        val login = mutableListOf<User>()
        while (resultSet.next()) {
            val id = resultSet.getInt("id")
            val name = resultSet.getString("name")
            val vorname = resultSet.getString("vorname")
            val password = resultSet.getString("password")
            val email = resultSet.getString("email")
            val phone = resultSet.getString("phone")
            val dni = resultSet.getString("dni")
            val erlauben = resultSet.getBoolean("erlauben")

            user = User(id, name, vorname, password, email, phone, dni, erlauben)
            login.add(user!!)
        }
    }
    @JvmName("getUsers1")
    fun getUsers() = users

    var email = ""

    fun getUser(id : Int): User? {
        var use: User? = null
        for (user in users){
            if (user.id == id){
                use = user
            }
        }
        return use
    }
}